<?php

namespace Drupal\Tests\formassembly\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main settings page loads with module enabled.
 *
 * @coversDefaultClass \Drupal\formassembly\Form\FormAssemblyEntityForm
 * @group formassembly
 */
class FormAssemblyLoadTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['formassembly'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Required setting.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'administer formassembly form entities',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the config page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('fa_form.settings'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
