<?php

declare(strict_types=1);

namespace Drupal\Tests\formassembly\Kernel;

use Drupal\Core\Render\HtmlResponse;
use Drupal\formassembly\Component\Render\FormassemblyHtmlResponseAttachmentsProcessor;
use Drupal\KernelTests\KernelTestBase;

/**
 * Kernel test methods of FormassemblyHtmlResponseAttachmentsProcessor.
 *
 * @coversDefaultClass \Drupal\formassembly\Component\Render\FormassemblyHtmlResponseAttachmentsProcessor
 *
 * @group formassembly
 */
final class FormassemblyHtmlResponseAttachmentsKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['formassembly'];

  /**
   * The processor under test.
   */
  protected FormassemblyHtmlResponseAttachmentsProcessor $processor;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->processor = new FormassemblyHtmlResponseAttachmentsProcessor(
      $this->container->get('html_response.attachments_processor'),
      $this->container->get('asset.resolver'),
      $this->container->get('config.factory'),
      $this->container->get('asset.css.collection_renderer'),
      $this->container->get('asset.js.collection_renderer'),
      $this->container->get('request_stack'),
      $this->container->get('renderer'),
      $this->container->get('module_handler'),
      $this->container->get('language_manager')
    );
  }

  /**
   * @covers ::processAttachments
   */
  public function testProcessAttachments() {
    // Create a response with fa_form_attachments attachments and ensure that
    // they are properly converted to html_head attachments. Also ensure that
    // the other html_head elements are added to create the necessary
    // javascript in the right order.
    $response = new HtmlResponse();
    $attachments['fa_form_attachments'] =
      [
        0 =>
          [
            '#type' => 'fa_form_inline_js',
            '#value' => "console.log('test')",
            '#weight' => 6,
          ],
        1 =>
          [
            '#type' => 'fa_form_external_css',
            '#rel' => 'stylesheet',
            '#href' => 'http://example.com/example.css',
            '#weight' => 1,
          ],
        2 =>
          [
            '#type' => 'fa_form_external_js',
            '#src' => 'http://example.com/example.js',
            '#weight' => 0,
          ],
      ];
    $response->setAttachments($attachments);
    $response = $this->processor->processAttachments($response);
    $processedAttachments = $response->getAttachments();
    $this->assertEquals('fa_form_external_js', $processedAttachments["html_head"][0][0]["#type"]);
    $this->assertEquals('fa_form_external_css', $processedAttachments["html_head"][1][0]["#type"]);
    $this->assertEquals('fa_form_inline_js', $processedAttachments["html_head"][2][0]["#type"]);
    $this->assertArrayNotHasKey('fa_form_attachments', $processedAttachments, 'The fa_form_attachments attachments are converted to html_head attachments.');
  }

  /**
   * @covers ::processAttachments
   */
  public function testProcessAttachmentsNoAdditions() {
    // Ensure that if there are no fa_form_attachments
    // nothing is added to the attachments.
    $response = new HtmlResponse();
    $response = $this->processor->processAttachments($response);
    $processedAttachments = $response->getAttachments();
    $this->assertEmpty($processedAttachments);
  }

}
